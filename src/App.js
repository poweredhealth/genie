import React, { Component } from 'react';
import './App.css';
import Checkbox from './Checkbox';
import axios from 'axios';

class App extends Component {
  constructor (props) {
    super(props);
    this.state = {data: [], pkTable: '', pkValue: 'auto', showMe: false};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.joinsClick = this.joinsClick.bind(this);
  }

  componentWillMount = () => {
    this.selectedCheckboxes = [];
  }

  joinsClick = (event) => {
    // create the join statements
    let pkTable = this.state.pkTable;
    this.selectedCheckboxes.forEach(c => {
      console.log(`${pkTable} joins to ${c}`);
    });
  }

  toggleCheckbox = label => {
    if (this.selectedCheckboxes.includes(label)) {
      this.selectedCheckboxes = this.selectedCheckboxes.filter(item => item !== label);
    } else {
      this.selectedCheckboxes.push(label);
    }
  }

  handleChange (event) {
    this.setState({[event.target.name]: event.target.value});
  }

  handleSubmit (event) {
    event.preventDefault();
    // reset variables
    this.setState({data: []});
    if (this.state.pkValue === '') this.setState({pkValue: 'auto'});
    this.selectedCheckboxes = [];

    let testPkValue = this.state.pkValue === 'auto' ? '' : '&test=' + this.state.pkValue;
    axios.get(`http://localhost:8000/find_fk?pkTable=${this.state.pkTable}${testPkValue}`).then(response => {
      this.setState({data: response.data.filter(r => r != null).map(this.createCheckboxesForFKResults)});
      console.log(response.data.filter(r => r != null));
    });
  }

  createCheckbox = label => {
    return <Checkbox
      label={label}
      handleCheckboxChange={this.toggleCheckbox}
      key={label}
    />;
  }

  createCheckboxes = items => {
    return items ? items.map(this.createCheckbox) : null;
  }

  createCheckboxesForFKResults = o => {
    // o is object like {fkTable: <tableName>, foreignKeys: [<key1>, <key2> ...]}
    let q = [];
    o.foreignKeys.forEach(fk => {
      q.push(o.fkTable + "." + fk);
    });
    if (q.length > 0) this.setState({showMe: true});
    return q.map(this.createCheckbox);
  }

  render () {
    return (
      <div className='button__container'>
        <form onSubmit={this.handleSubmit}>
          <div>
            <label>pkTable</label>
            <input type="text" name="pkTable" value={this.state.pkTable} onChange={this.handleChange}/>
          </div>
          <div>
            <label>pkValue</label>
            <input type="text" name="pkValue" value={this.state.pkValue} onChange={this.handleChange}/>
          </div>
        </form>
        <div>
          <button className='button' onClick={this.handleSubmit}>
	          Get
	        </button>
        </div>
        {this.state.data}
        <div>
          <button className='button' style={{display: this.state.data.length > 0 ? 'inline' : 'none'}} onClick={this.joinsClick}>Create Joins</button>
        </div>
      </div>
    );
  }
}
export default App